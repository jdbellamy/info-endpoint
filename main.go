package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func main() {
	addr := fmt.Sprintf(":%d", 8888)

	logrus.Infof("serving requests on %s", addr)

	r := mux.NewRouter()

	r.Path("/info").Methods("GET").HandlerFunc(Info)

	http.ListenAndServe(addr, r)
}

type info struct {
	Time time.Time
}

func Info(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(info{
		Time: time.Now(),
	})
}
